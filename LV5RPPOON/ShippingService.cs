﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV5RPPOON
{
    class ShippingService
    {
        private double ServicePrice;

        public ShippingService(double ServicePrice) { this.ServicePrice = ServicePrice; }

        public double Price(double weight)
        {
            return ServicePrice * weight;
        }

       
    }
}
