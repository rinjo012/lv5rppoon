﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5RPPOON
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IShipable> list = new List<IShipable>();
           
            Product Lenovo = new Product("Lenovo", 1772, 2);
            list.Add(Lenovo);
           
            Product HP = new Product("HP", 2399.99, 2);
            list.Add(HP);


            ShippingService Shipping = new ShippingService(9);
            double weight = 0;
            
            foreach (IShipable item in list)
            {

                weight += item.Weight;

                Console.WriteLine(item.Description());

            }

            Console.WriteLine( Shipping.Price(weight) + " HRK");

        }
    }
}
